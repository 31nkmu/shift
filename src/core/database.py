from typing import AsyncGenerator

from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, \
    create_async_engine
from sqlalchemy.orm import DeclarativeBase

from src.core.config import \
    async_db_engine_settings as DATABASE_URL


class Base(DeclarativeBase):
    pass


# создаем подключение к базе данных асинхронно
engine = create_async_engine(DATABASE_URL)
async_session_maker = async_sessionmaker(engine, expire_on_commit=False)


async def get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as session:
        yield session
