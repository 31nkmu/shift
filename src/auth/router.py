from fastapi import Depends, APIRouter

from .models import User
from .schemas import UserCreate, UserRead, UserUpdate, UserSalary
from .manager import auth_backend, current_active_user, fastapi_users

auth_router = APIRouter()
auth_tag = "auth"

auth_router.include_router(
    fastapi_users.get_auth_router(auth_backend),
    prefix="/auth/jwt",
    tags=[auth_tag]
)
auth_router.include_router(
    fastapi_users.get_register_router(UserRead, UserCreate),
    prefix="/auth",
    tags=[auth_tag],
)
auth_router.include_router(
    fastapi_users.get_reset_password_router(),
    prefix="/auth",
    tags=[auth_tag],
)

auth_router.include_router(
    fastapi_users.get_verify_router(UserRead),
    prefix="/auth",
    tags=[auth_tag],
)
auth_router.include_router(
    fastapi_users.get_users_router(UserRead, UserUpdate),
    prefix="/users",
    tags=["users"],
)


@auth_router.get("/salary_data", tags=['salary'])
async def salary_data(user: User = Depends(current_active_user)):
    return {'msg': {'current_salary': user.current_salary, 'promotion_date': user.promotion_date}, 'status': 200}
