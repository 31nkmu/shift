import decimal
from datetime import date

from fastapi_users_db_sqlalchemy import SQLAlchemyBaseUserTable
from sqlalchemy import String, Boolean, DECIMAL, Date

from sqlalchemy.orm import Mapped, mapped_column

from src.core.database import Base


class User(SQLAlchemyBaseUserTable[int], Base):
    __tablename__ = 'user_account'

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True, index=True)
    username: Mapped[str] = mapped_column(String(320), nullable=True)
    email: Mapped[str] = mapped_column(String(length=320), unique=True, index=True, nullable=False)
    hashed_password: Mapped[str] = mapped_column(String(length=1024), nullable=False)
    is_active: Mapped[bool] = mapped_column(Boolean, default=True, nullable=False)
    is_superuser: Mapped[bool] = mapped_column(Boolean, default=False, nullable=False)
    is_verified: Mapped[bool] = mapped_column(Boolean, default=False, nullable=False)
    current_salary: Mapped[decimal.Decimal] = mapped_column(DECIMAL, nullable=False)
    promotion_date: Mapped[date] = mapped_column(Date, nullable=True)

