import decimal
from datetime import date
from typing import Optional

from fastapi_users import schemas


class UserRead(schemas.BaseUser[int]):
    pass


class UserCreate(schemas.BaseUserCreate):
    # username: Optional[str]
    current_salary: Optional[decimal.Decimal]
    promotion_date: Optional[date]


class UserUpdate(schemas.BaseUserUpdate):
    pass


class UserSalary(schemas.BaseModel):
    current_salary: decimal.Decimal
    promotion_date: date

    class Config:
        orm_mode = True
