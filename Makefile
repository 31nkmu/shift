.SILENT:

## Миграции

.PHONY: migrate-up ## Проводит миграции до последней версии
migrate-up:
	poetry run alembic upgrade head

.PHONY: migrate-create
migrate-create: ## Создает миграции
	poetry run alembic revision --autogenerate -m $(name)

.PHONY: migrate-down
migrate-down: ## Revert to a previous version
	poetry run alembic downgrade $(revision)


## Проект

.PHONY: run
run: ## запуск проекта
	poetry run gunicorn --reload --bind $(HOST):$(BACKEND_PORT) \
	--worker-class uvicorn.workers.UvicornWorker \
	--workers $(WORKERS) --log-level $(LEVEL) src.main:app


## Докер

.PHONY: compose-up
compose-up: ## Создает и запускает контейнеры
	docker-compose -f docker/docker-compose.yml --env-file docker/.env up -d --build

.PHONY: compose-logs
compose-logs: ## Логи контейнера
	docker-compose -f docker/docker-compose.yml --env-file docker/.env logs -f

.PHONY: compose-ps
compose-ps: ## Список контейнеров
	docker-compose -f docker/docker-compose.yml --env-file docker/.env ps

.PHONY: compose-ls
compose-ls: ## Список запущенных контейнеров
	docker-compose -f docker/docker-compose.yml --env-file docker/.env ls

.PHONY: compose-exec
compose-exec: ## Использовать команду в запущенном контейнере проекта
	docker-compose -f docker/docker-compose.yml --env-file docker/.env exec backend bash

.PHONY: compose-start
compose-start: ## Запуск сервисов
	docker-compose -f docker/docker-compose.yml --env-file docker/.env start

.PHONY: compose-restart
compose-restart: ## Перезапуск сервисов
	docker-compose -f docker/docker-compose.yml --env-file docker/.env restart

.PHONY: compose-stop
compose-stop: ## Остановка сервисов
	docker-compose -f docker/docker-compose.yml --env-file docker/.env stop

.PHONY: compose-down
compose-down: ## Остановка и удаление сервисов
	docker-compose -f docker/docker-compose.yml --env-file docker/.env down --remove-orphans

.PHONY: docker-rm-volume
docker-rm-volume: ## Удаление зависимостей базы данных
	docker-compose -f docker/docker-compose.yml --env-file docker/.env down -v

.PHONY: docker-clean
docker-clean: ## Удаление неиспользованных данных
	docker system prune